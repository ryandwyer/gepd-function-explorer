General guard

    Wait until not moving
    If Bond is in sight, assign 0006 (take a shot)
    If shot, assign 0008 (jog to Bond)
    If heard Bond, assign 000b (alerted cloneable guard)
    If a shot has been fired nearby, randomly assign 000a (look around then jog to Bond) or 0006 (take a shot)
    If another guard killed in sight, assign 0008 (jog to Bond)
    If not busy, 1 in 255 chance of 0003 (do idle animation)
