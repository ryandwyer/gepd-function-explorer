Civilian/trusting enemy

Face current player

Wait for you to shoot someone nearby (goto 83 - become hostile) or standing in front of them (13 - greet player)

    Label 13 - greeting
    If Chicago, say something like "Greetings, citizen" then jump to return function
    For other levels:
        Set some property
        If player is unarmed, say "How's things?" or similar and jump to return function
        If player is armed, say "Where did you get that?" or similar and jump to return function

    Label 83 - become hostile
    Set alertness to 100 and assign 0006
