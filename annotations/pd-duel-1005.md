Main logic

Wait for guard 0 to die
"Well done"
Set flag 9 - objective 1 complete?
Wait 2 seconds
If difficulty <= A, remove current function
Set flag 12

Wait for guard 1 to die
"Well done"
Set flag 10 - objective 2 complete?
Wait 2 seconds
If difficulty <= SA, remove current function
Set flag 13

Wait for guard 2 to die
"Well done"
Set flag 11 - objective 2 complete?
Wait 2 seconds