Check guard capture failed

Looks like you could trick the game into failing you by not subduing any guard, run in Carrington area and close the door behind you. Objective will fail, and if you exit again and subdue a guard it won't recognise it.