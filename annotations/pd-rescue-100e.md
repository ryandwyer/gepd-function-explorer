End timer

Wait for last room door to be opening
Set flag 22
Start X music
Start countdown timer (60 seconds)
Wait for flag 23 or timer expired

Flag 23:
Cancel timer
Exit

Timer expired:
Cancel timer
Set flag 24
"Elvis has been killed"
Exit