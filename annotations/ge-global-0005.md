General guard

Go into loop checking for any of these:

* If Bond is in sight, assign 0006 (take a shot) then assign 0002
* If actor has been shot, assign 0008 (jog to Bond and attack if seen) then assign 0002
* If a shot has been fired, 000a or 0006 (50% chance of looking around first) then assign 0002
* If actor shot in sight, assign 0008 (jog to Bond and attack if seen) then assign 0002
