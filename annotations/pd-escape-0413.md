Jon hanger

Flag 14 = Jon started running to first terminal
Flag 15 = ?
Flag 22 = ?
Flag 23 = Inner hanger door opening
Flag 24 = Terminal 2 done?
Flag 26 = ?
Flag 27 = Jon started running to first terminal
Flag 28 = X music started
Flag 29 = ?

Flag 23 = inner door opening/opened
Flag 24 = outer door opening/opened
Flag 28 = x music started/terminals ready


"Cover me while I open the doors"
Allow 2 seconds for speech to play
Set flags 27 and 14
Start running to terminal 2
Over the next 4 seconds, if Jon's movement stops goto 06 (ie. at terminal 2)
After 4 seconds, start doing more checks:
- flag_28 true (impossible on first iteration)? goto 32
- on ramp? goto 33, which starts x music and sets flag_28
- within 30 units of Jo? Jo takes over
- stopped moving? (ie. at terminal?) goto 06

Terminal 1 = pads 0171, 03fd, 03f7
Terminal 2 = pads 048e, 016f, 03f8