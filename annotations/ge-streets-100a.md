Check civilian casualties

Actor byte 2 is a count of how many remaining civilians can be killed before the mission is failed.

Presumably, when a civilian is deleted for being too far away, it would trip up this function and it would count it as a kill. So to get around it, when the game deletes a civilian it sets flag 14, and this function checks that and increases the counter, effectively making them not count.