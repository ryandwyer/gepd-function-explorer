Spawn ramping

Wait for flag 12 (enable spawning)
Unset flags 13-18, allowing those actors to spawn
Wait 3 minutes
Set flags 16, 17, 18 (disabling those actors, and inadvertently completing the Nat objective)
Wait 10 minutes
Turn off other spawning