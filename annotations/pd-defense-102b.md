Safe

If PA:
Make safe immune to explosives
Wait for objective 2
If safe destroyed, exit
While true:
    If play is in safe room and has laser equipped, make vulnerable, else make invulnerable