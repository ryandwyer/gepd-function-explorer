Hologram hostage

Wait for hologram room door to open
If autoguns objective incomplete, set flag 27
"There's no escape for you"
Wait up to 2 seconds or until Jo in line of sight
"Hah, catch me if you can"
Spawn guard at pad 0104 with function 041C
Wait 10 frames
Spawn guard at pad 0105 with function 041D
Wait 11 frames
Spawn guard at pad 0106 with function 041E
Assign functions 0420, 0421, 0422 to 3 takers
Jog to pad 0107
When stopped, do crouching animation
When flag 19 is set (takers killed), assign function 0426 to $this