Respawn

Wait until actor F9 finished death animation
Respawn actor $this
Remove actor $this

Seems like the actor would respawn continuously, but this method was abandoned in favour of 0406 (detecting via pad and using a spawn limit)