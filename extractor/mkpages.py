import base64, glob, json, os, lib.HtmlGenerator, lib.Parser, sys
from operator import itemgetter

games = [];
if '--ge' in sys.argv:
    games.append('ge')
if '--pd' in sys.argv:
    games.append('pd')
if len(games) == 0:
    games = ['ge','pd']

for game in games:
    for file in glob.glob('public/%s-*.html' % game):
        os.remove(file)

    """
    Step 1: Read the functions from a JSON file.
    (Due to copyright, the game ROMs cannot be included in this project)
    """
    fp = open('extractor/%s.json' % game, 'r')
    functions = json.load(fp)
    fp.close()
    functions = sorted(functions, key=itemgetter('stage_id', 'id'))

    """
    Step 2: The parser breaks the function down into instructions and adds
    descriptions for each one.
    """
    parser = lib.Parser.load(game)
    for function in functions:
        function['raw'] = base64.b64decode(function['raw'])
        parser.setFunction(function)
        function['instructions'] = parser.parse()

    """
    Step 3: The generator combines the instructions with the user-added annotations
    and writes the HTML files.
    """
    generator = lib.HtmlGenerator.load(game)
    generator.setFunctions(functions)
    generator.setAnnotationsDirectory('annotations')
    generator.setOutputDirectory('public')
    generator.generate()

generator.generateIndexFile()
